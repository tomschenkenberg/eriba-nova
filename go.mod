module tomschenkenberg.gitlab.io/eriba-nova

go 1.13

require (
	github.com/halogenica/beautifulhugo v0.0.0-20210106080152-99ca240e9977 // indirect
	github.com/yihui/hugo-xmin v0.0.0-20201016020520-4100270f6fe5 // indirect
)
